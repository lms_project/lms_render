package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"lms_ulab/models"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type UserRepo struct {
	db *pgxpool.Pool
}

func NewUserRepo(db *pgxpool.Pool) *UserRepo {
	return &UserRepo{
		db: db,
	}
}

func (u *UserRepo) Create(ctx context.Context, req *models.CreateUser) (*models.User, error) {
	id := uuid.New().String()

	var (
		query = `INSERT INTO "user"(
					id,
					first_name,
					last_name,
					email,
					password,
					role,
					updated_at
		)VALUES($1,$2,$3,$4,$5,$6,NOW())

		`
	)

	_, err := u.db.Exec(ctx,
		query,
		id,
		req.FirstName,
		req.LastName,
		req.Email,
		req.Password,
		"student",
	)
	if err != nil {
		return nil, err
	}

	resp, err := u.GetByID(ctx, &models.UserPrimaryKey{Id: id})
	if err != nil {
		return nil, err
	}

	return resp, nil

}

func (u *UserRepo) GetByID(ctx context.Context, req *models.UserPrimaryKey) (*models.User, error) {

	var (
		query = `
			SELECT
					"id",
					"first_name",
					"last_name",
					"email",
					"password",
					"role",
					"created_at",
					"updated_at"
			FROM "user"
			WHERE "id"=$1 
		`
	)

	var (
		id         sql.NullString
		first_name sql.NullString
		last_name  sql.NullString
		email      sql.NullString
		password   sql.NullString
		role       sql.NullString
		created_at sql.NullString
		updated_at sql.NullString
	)

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&first_name,
		&last_name,
		&email,
		&password,
		&role,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &models.User{
		Id:        id.String,
		FirstName: first_name.String,
		LastName:  last_name.String,
		Email:     email.String,
		Password:  password.String,
		Role:      role.String,
		CreatedAt: created_at.String,
		UpdatedAt: updated_at.String,
	}, nil
}

func (u *UserRepo) GetList(ctx context.Context, req *models.GetListUserRequest) (*models.GetListUserResponse, error) {
	var (
		resp   models.GetListUserResponse
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		sort   = " ORDER BY created_at DESC"
	)

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	// if len(req.Search) > 0 {
	// 	where += " AND first_name ILIKE" + " '%" + req.Search + "%'"
	// }

	var query = `
		SELECT
			COUNT(*) OVER(),
			id,
			first_name,
			last_name,
			email,
			password,
			role,
			created_at,
			updated_at	
		FROM "user"
	`

	query += where + sort + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			Id        sql.NullString
			FirstName sql.NullString
			LastName  sql.NullString
			Email     sql.NullString
			Password  sql.NullString
			Role      sql.NullString
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&Id,
			&FirstName,
			&LastName,
			&Email,
			&Password,
			&Role,
			&CreatedAt,
			&UpdatedAt,
		)
		if err != nil {
			return nil, err
		}

		resp.Users = append(resp.Users, &models.User{
			Id:        Id.String,
			FirstName: FirstName.String,
			LastName:  LastName.String,
			Email:     Email.String,
			Password:  Password.String,
			Role:      Role.String,
			CreatedAt: CreatedAt.String,
			UpdatedAt: UpdatedAt.String,
		})

	}

	return &resp, nil
}

func (u *UserRepo) Update(ctx context.Context, req *models.UpdateUser) (int64, error) {

	query := `
		UPDATE "user"
			SET
				first_name = $2,
				last_name = $3,
				email = $4,
				password = $5,
				updated_at = NOW()
		WHERE id = $1
	`
	rowsAffected, err := u.db.Exec(ctx,
		query,
		req.Id,
		req.FirstName,
		req.LastName,
		req.Email,
		req.Password,
	)
	if err != nil {
		return 0, err
	}

	return rowsAffected.RowsAffected(), nil
}

func (u *UserRepo) Delete(ctx context.Context, req *models.UserPrimaryKey) error {
	_, err := u.db.Exec(ctx, `DELETE FROM "user" WHERE id = $1`, req.Id)
	return err
}

func (u *UserRepo) GetByEmail(ctx context.Context, req *models.UserPrimaryEmail) (*models.User, error){

	var (
		query = `
			SELECT
					"id",
					"first_name",
					"last_name",
					"email",
					"password",
					"role",
					"created_at",
					"updated_at"
			FROM "user"
			WHERE "email"= $1 
		`
	)

	var (
		id         sql.NullString
		first_name sql.NullString
		last_name  sql.NullString
		email      sql.NullString
		password   sql.NullString
		role       sql.NullString
		created_at sql.NullString
		updated_at sql.NullString
	)

	err := u.db.QueryRow(ctx, query, req.Email).Scan(
		&id,
		&first_name,
		&last_name,
		&email,
		&password,
		&role,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &models.User{
		Id:        id.String,
		FirstName: first_name.String,
		LastName:  last_name.String,
		Email:     email.String,
		Password:  password.String,
		Role:      role.String,
		CreatedAt: created_at.String,
		UpdatedAt: updated_at.String,
	}, nil
}





