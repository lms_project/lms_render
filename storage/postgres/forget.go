package postgres

import (
	"context"
	"database/sql"
	"lms_ulab/models"

	"github.com/jackc/pgx/v4/pgxpool"
)

type ForgetRepo struct {
	db *pgxpool.Pool
}

func NewForgetRepo(db *pgxpool.Pool) *ForgetRepo {
	return &ForgetRepo{
		db: db,
	}
}

func (r *ForgetRepo) Create(ctx context.Context, req *models.ForgetPasswords) error {
	var (
		query = `
		
		INSERT INTO "forget"(
			"email",
			"code",
			"created_at"
		)VALUES($1,$2,NOW())

		`
	)
	_, err := r.db.Exec(
		ctx,
		query,
		req.Email,
		req.Code,
	)
	if err != nil {
		return nil
	}
	return nil
}

func (r *ForgetRepo) GetById(ctx context.Context, req *models.ForgetPasswordPrimaryKey) (*models.Forget, error) {
	var (
		query = `
		SELECT 
			"email",
			"code",
			"created_at"
		FROM "forget"
		WHERE "email"=$1

		`
	)
	var (
		email      sql.NullString
		code       sql.NullString
		created_at sql.NullString
	)
	err := r.db.QueryRow(ctx, query, req.Email).Scan(
		&email,
		&code,
		&created_at,
	)

	if err != nil {
		return nil, err
	}
	return &models.Forget{
		Email:     email.String,
		Code:      code.String,
		CreatedAt: created_at.String,
	}, nil

}

func (r *ForgetRepo) Delete(ctx context.Context, req *models.ForgetPasswordPrimaryKey) error {
	_, err := r.db.Exec(ctx, "DELETE FROM forget WHERE email = $1", req.Email)
	return err
}
