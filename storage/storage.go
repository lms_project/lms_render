package storage

import (
	"context"
	"lms_ulab/models"
)

type StorageI interface {
	User() UserRepoI
	Forget() ForgetRepoI

}

type ForgetRepoI interface{
	Create(ctx context.Context, req *models.ForgetPasswords) (error)
	Delete(ctx context.Context, req *models.ForgetPasswordPrimaryKey) error
	GetById(ctx context.Context, req *models.ForgetPasswordPrimaryKey) (*models.Forget,error)
}

type UserRepoI interface {
	Create(ctx context.Context, req *models.CreateUser) (*models.User, error)
	GetByID(ctx context.Context, req *models.UserPrimaryKey) (*models.User, error)
	GetList(ctx context.Context, req *models.GetListUserRequest) (*models.GetListUserResponse, error)
	Update(ctx context.Context, req *models.UpdateUser) (int64, error)
	Delete(ctx context.Context, req *models.UserPrimaryKey) error
	GetByEmail(ctx context.Context, req *models.UserPrimaryEmail) (*models.User, error)

}
