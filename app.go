package api

import (
	"context"
	"fmt"
	"lms_ulab/config"
	"lms_ulab/models"
	"lms_ulab/storage/postgres"
	"math/rand"
	"net/http"
	"net/smtp"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

var (
	app *gin.Engine
)

func init() {
	app = gin.New()
	r := app.Group("/api")
	MyRoute(r)
}

func Handler(w http.ResponseWriter, r *http.Request) {
	app.ServeHTTP(w, r)

}

func MyRoute(r *gin.RouterGroup) {
	r.POST("/register", func(c *gin.Context) {
		RegisterUser(c)
	})
	r.POST("/login", func(c *gin.Context) {
		Login(c)
	})
	r.POST("/forget-password", func(c *gin.Context) {
		ForgetPasswords(c)
	})
	r.POST("/verify", func(c *gin.Context) {
		Verify(c)
	})
	r.PUT("/reset-password", func(c *gin.Context) {
		ResetPassword(c)
	})

}

func RegisterUser(c *gin.Context) {
	var user models.CreateUser
	err := c.ShouldBindJSON(&user)
	if err != nil {
		c.JSON(500, "failed to marshall json file")
		return
	}
	var cfg = config.Load()
	strg, err := postgres.NewConnectionPostgres(&cfg)
	if err != nil {
		panic(err)
	}

	resp, err := strg.User().Create(context.Background(), &user)

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	c.JSON(200, resp)

}

func Login(c *gin.Context) {

	var email = c.Query("email")
	var password = c.Query("password")

	var cfg = config.Load()
	strg, err := postgres.NewConnectionPostgres(&cfg)
	if err != nil {
		panic(err)
	}

	user, err := strg.User().GetByEmail(context.Background(), &models.UserPrimaryEmail{
		Email: email,
	})
	if err != nil {
		c.JSON(500, err)
		return
	}
	if password != user.Password {
		c.JSON(http.StatusBadRequest, "password or email is wrong")
	}
	c.JSON(200, "success")

}

func ForgetPasswords(c *gin.Context) {
	var email = c.Query("email")
	var createforget models.ForgetPasswords

	from := "yerimbetovnodirbek0669@gmail.com"
	password := "ptwnrrixehgpdaxt"

	to := []string{
		email,
	}
	var cfg = config.Load()
	pgStore, err := postgres.NewConnectionPostgres(&cfg)
	if err != nil {
		panic(err)
	}
	_, err = pgStore.User().GetByEmail(context.Background(), &models.UserPrimaryEmail{Email: email})
	if err != nil {
		c.JSON(http.StatusInternalServerError, "This email does not exist")
		return
	}

	err = pgStore.Forget().Delete(context.Background(), &models.ForgetPasswordPrimaryKey{Email: email})
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	smtpHost := "smtp.gmail.com"
	smtpPort := "587"
	msg := rand.Intn(90000) + rand.Intn(10000)
	code := strconv.Itoa(msg)

	subject := "Authentication code"
	message := []byte("Subject: " + subject + "\r\n" + "\r\n" + code + "\r\n")

	auth := smtp.PlainAuth("", from, password, smtpHost)
	stringmsg := fmt.Sprintf("%v", msg)

	createforget.Email = email
	createforget.Code = stringmsg

	err = pgStore.Forget().Create(context.Background(), &createforget)
	if err != nil {
		return
	}

	err = smtp.SendMail(smtpHost+":"+smtpPort, auth, from, to, message)
	if err != nil {
		fmt.Println(err)
		return
	}
	c.JSON(200, "success")
}

func Verify(c *gin.Context) {

	var email = c.Query("email")
	var code = c.Query("code")

	var cfg = config.Load()

	pgStore, err := postgres.NewConnectionPostgres(&cfg)
	if err != nil {
		panic(err)
	}
	resp, err := pgStore.Forget().GetById(context.Background(), &models.ForgetPasswordPrimaryKey{Email: email})

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}

	var (
		day     string
		hour    string
		minutes string
		sekund  string
	)
	for i := 8; i < 10; i++ {
		day += string(resp.CreatedAt[i])
	}
	for i := 11; i < 13; i++ {
		hour += string(resp.CreatedAt[i])
	}
	for i := 14; i < 16; i++ {
		minutes += string(resp.CreatedAt[i])
	}
	for i := 17; i < 19; i++ {
		sekund += string(resp.CreatedAt[i])
	}
	dayInt, _ := strconv.Atoi(day)
	hourInt, _ := strconv.Atoi(hour)
	minutesInt, _ := strconv.Atoi(minutes)
	sekundInt, _ := strconv.Atoi(sekund)
	if resp.Code != code {
		c.JSON(http.StatusInternalServerError, "Wrong code,try again")
		return
	}
	if resp.Code == code && dayInt == time.Now().Day() && hourInt == time.Now().Hour() && minutesInt*60+sekundInt+60 < time.Now().Minute()*60+time.Now().Second() {
		c.JSON(http.StatusInternalServerError, "expired is up,try again")
		err = pgStore.Forget().Delete(context.Background(), &models.ForgetPasswordPrimaryKey{Email: resp.Email})
		if err != nil {
			c.JSON(http.StatusInternalServerError, err)
			return
		}
		return
	} else {
		c.JSON(http.StatusOK, "success")
	}

	err = pgStore.Forget().Delete(context.Background(), &models.ForgetPasswordPrimaryKey{Email: resp.Email})
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}
}

func ResetPassword(c *gin.Context) {
	var email = c.Query("email")
	var password = c.Query("password")

	var cfg = config.Load()
	strg, err := postgres.NewConnectionPostgres(&cfg)
	if err != nil {
		panic(err)
	}
	user, err := strg.User().GetByEmail(context.Background(), &models.UserPrimaryEmail{
		Email: email,
	})
	if err != nil {
		c.JSON(500, err)
		return
	}
	_, err = strg.User().Update(context.Background(), &models.UpdateUser{
		Id:        user.Id,
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
		Password:  password,
	})
	if err != nil {
		c.JSON(500, err)
		return
	}
	resp, err := strg.User().GetByID(context.Background(), &models.UserPrimaryKey{Id: user.Id})
	if err != nil {
		c.JSON(500, err)
		return
	}

	c.JSON(200, resp)

}
