package models

type ForgetPasswords struct {
	Email string `json:"email"`
	Code  string  `json:"code"`
}

type ForgetPasswordPrimaryKey struct {
	Email     string `json:"email"`
}

type Forget struct {
	Email     string `json:"email"`
	Code      string  `json:"code"`
	CreatedAt string `json:"created_at"`
}

type Login struct {
	Email string `json:"email"`
	Password string `json:"password"`
}
