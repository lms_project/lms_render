CREATE TABLE "user" (
  "id" UUID PRIMARY KEY NOT NULL,
  "first_name" VARCHAR(25) NOT NULL,
  "last_name" VARCHAR(25) NOT NULL,
  "email" VARCHAR(255) UNIQUE NOT NULL,
  "password" VARCHAR(255) NOT NULL,
  "role" VARCHAR(25) NOT NULL,
  "created_at" TIMESTAMP DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" TIMESTAMP
);

CREATE TABLE "forget" (
  "email" VARCHAR(255) NOT NULL,
  "code" VARCHAR(10) NOT NULL,
  "created_at" TIMESTAMP
);